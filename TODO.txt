Validation
1. Registration errors - UNRESOLVED!
2. Admin should be able to check the average book rating. RESOLVED!
3. Improve the rating calculation logic. RESOLVED!

Optional:
1. Display all users - ADMIN - with or without the ADMIN?!
2. Admin can block users - RESOLVED!
3. List all books + search amongst them - where to put this method?
4. Auto login after successful registration
5. Validation messages across the site
6. Refactoring
7. Adding comments over the controllers + Configuration
8. Admin cannot block yourself. Disable the button - RESOLVED!
9. Extract Footer